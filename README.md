# Diagrams
> These are examples of simple charts written in Python

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Contact](#contact)

## General info
This repository is part of my learning path in programming. Idea of preparing charts comes from webinar of Akademia Kodu.

## Technologies
* Python - version 3.8
* Libraries: NumPy, Matplotlib

## Code Examples
import numpy
import matplotlib
import pylab

x = [1, 2, 3]
y = [1, 2, 3]

pylab.plot(x, y)
pylab.show()


To-do list:
* Learn how to write more advanced charts

## Status
Project is: _in progress_


## Contact
Created by [@martarys](http://pc54782.wsbpoz.solidhost.pl/wordpress/) - feel free to contact me!
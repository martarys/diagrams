import numpy
import matplotlib
import pylab

x = range(-5, 6)
y = []

for i in x:
    y.append(2*i)

print(y)
pylab.title("Wykres funkcji 2x")
pylab.plot(x, y)
pylab.grid(True)
pylab.show()